function calculateBirthDate(){

    const NAME = document.getElementById("name").value;

    const BIRTH_YEAR = parseInt( document.getElementById("year").value );
    const BIRTH_MONTH = parseInt( document.getElementById("month").value );
    const BIRTH_DATE = parseInt( document.getElementById("date").value );

    const CURRENT_DATE = new Date();
    const CURRENT_YEAR= parseInt(CURRENT_DATE.getFullYear());
    const CURRENT_MONTH  = parseInt(CURRENT_DATE.getMonth()+1);
    const CURRENT_DAY = parseInt(CURRENT_DATE.getDate());

    let ageYears = CURRENT_YEAR - BIRTH_YEAR;
    let ageMonths;

    const AGE_ELEMENT = document.getElementById("age");

    if( validateInputs(NAME, BIRTH_YEAR, BIRTH_MONTH, BIRTH_DATE) ){
        AGE_ELEMENT.value  = "";
    }
    else{       

        if(CURRENT_MONTH < BIRTH_MONTH){
            ageYears--;
            ageMonths = CURRENT_MONTH + (12- BIRTH_MONTH);
            if(CURRENT_DAY < BIRTH_DATE){
                ageMonths--;
            }
        }
        else{
            ageMonths = CURRENT_MONTH - BIRTH_MONTH;
            if(CURRENT_DAY < BIRTH_DATE){
                ageMonths--;
            }      
        }

        if(ageMonths == 0){
            AGE_ELEMENT.innerHTML  = NAME +" is "+ ageYears +" years old";
        }
        else if(ageYears == 0){
            AGE_ELEMENT.innerHTML  = NAME +" is "+ ageMonths +" months old";
        }
        else{
            AGE_ELEMENT.innerHTML  = NAME +" is "+ ageYears +" years and "+ ageMonths +" months old";
        }

    }
    
}

function validateInputs(name, birthYear, birthMonth, birthDate){

    let result = false;
    const ERROR_ELEMENT_NAME = document.getElementById("alert_name_req");
    const ERROR_ELEMENT_DATE = document.getElementById("alert_date_req");

    if(name == ""){
        ERROR_ELEMENT_NAME.innerHTML  = "Name Field is required";  

        ERROR_ELEMENT_NAME.style.display = 'block'; 
        setTimeout(function () {
            ERROR_ELEMENT_NAME.style.display='none';
        }, 5000);   

        result = true;
    }
    if(isNaN(birthYear) || isNaN(birthMonth) || isNaN(birthDate) ){
        ERROR_ELEMENT_DATE.innerHTML  = "Date is required";

        ERROR_ELEMENT_DATE.style.display = 'block'; 
        setTimeout(function () {
            ERROR_ELEMENT_DATE.style.display='none';
        }, 5000);   

        result = true;
    }

    return result;

}
