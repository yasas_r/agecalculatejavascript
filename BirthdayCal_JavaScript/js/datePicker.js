window.onload = function()
{
    initialYears();
    initialDays();
};

const CURRENT_DATE = new Date();
const CURRENT_YEAR = CURRENT_DATE.getFullYear();

function initialYears(){
    const PAST_YEARS = CURRENT_YEAR - 100; // years - shows in year drop down

    let yearSelector = document.getElementById("year");

    for (let year = CURRENT_YEAR; year >= PAST_YEARS; year--) {       
        let option = document.createElement("option");
        option.text = year;
        option.value = year;

        yearSelector.add(option);
    }
}

function initialDays(){
    let dateSelector = document.getElementById("date");
    for (let date = 1; date <= 31; date++) {
        let option = document.createElement("option");
        option.text = date;
        option.value = date;
        option.id = 'date'+date;

        dateSelector.add(option);
    }
}

document.getElementById("year").onchange = function() {

    const MONTH_ELEMENT = document.getElementById("month");
    const DATE_ELEMENT = document.getElementById("date");

    MONTH_ELEMENT.value  = "";
    DATE_ELEMENT.value  = "";

    const YEAR = $(this).val();   
    const CURRENT_MONTH  = parseInt( CURRENT_DATE.getMonth() + 1 );

    setMonths(YEAR, CURRENT_YEAR, CURRENT_MONTH);

    MONTH_ELEMENT.disabled = false; //month enable
    DATE_ELEMENT.disabled = true; // dates disable

    MONTH_ELEMENT.onchange = function() {

        DATE_ELEMENT.value  = "";
        
        const MONTH = parseInt(MONTH_ELEMENT.value);
        const CURRENT_DAY = parseInt(CURRENT_DATE.getDate());  

        setDates(YEAR, MONTH, CURRENT_YEAR, CURRENT_MONTH, CURRENT_DAY);
            
        DATE_ELEMENT.disabled = false; //dates enable
    };

};

        /**checking current year with inputted year to disable next months */
function setMonths(year, currentYear, currentMonth){
    if(year == currentYear){
        for (let index = currentMonth+1; index <= 12; index++) {    
            document.getElementById('month'+index+'').disabled=true;
        }
    }
    else{
        for (let index =1; index <= 12; index++) {
            document.getElementById('month'+index+'').disabled=false;  
        }
    }
}

function setDates(year, month, currentYear, currentMonth, currentDay){

    let date29_element = document.getElementById("date29");
    let date30_element = document.getElementById("date30");
    let date31_element = document.getElementById("date31");

    /** hide and show date drop down according to inputted month*/
    if( [8,9,10,11,12].indexOf(month) == -1 ){   
        if(year % 4 == 0 && month == 2){
            date29_element.style.display = 'block'; 
            date30_element.style.display = 'none'; 
            date31_element.style.display = 'none'; 
        } else if(month == 2){
            date29_element.style.display = 'none'; 
            date30_element.style.display = 'none'; 
            date31_element.style.display = 'none';
        } else if(month % 2 == 0){
            date29_element.style.display = 'block'; 
            date30_element.style.display = 'block'; 
            date31_element.style.display = 'none';          
        }
        else{
            date29_element.style.display = 'block'; 
            date30_element.style.display = 'block'; 
            date31_element.style.display = 'block';              
        }         
    }    
    else{
        if(month % 2 == 0){
            date29_element.style.display = 'block'; 
            date30_element.style.display = 'block'; 
            date31_element.style.display = 'block';           
        }
        else{
            date29_element.style.display = 'block'; 
            date30_element.style.display = 'block'; 
            date31_element.style.display = 'none';             
        }
    }

    /**checking current month with inputted month to disable next days */
    if(year == currentYear && month == currentMonth){
        for (let index = currentDay+1; index <= 31; index++) {
            document.getElementById('date'+index+'').disabled=true;       
        }
    }
    else{
        for (let index = 1; index <= 31; index++) {
            document.getElementById('date'+index+'').disabled=false;        
        }
    }
}










