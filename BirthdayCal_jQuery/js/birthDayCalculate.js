function calculateBirthDate(){

    var name = $("#name").val();

    var birthYear = parseInt($("#year").val());
    var birthMonth = parseInt($("#month").val());
    var birthDate = parseInt($('#date').val());

    

    var currentDate = new Date();
    var currentYear = parseInt(currentDate.getFullYear());
    var currentMonth  = parseInt(currentDate.getMonth()+1);
    var currentDate = parseInt(currentDate.getDate());

    var ageYears = currentYear - birthYear;
    var ageMonths;

    if(validateInputs(name,birthYear,birthMonth,birthDate)){
        $('#age').html("");
    }

    else{

        if(currentMonth < birthMonth){
            ageYears--;
            ageMonths = currentMonth + (12- birthMonth);
            if(currentDate < birthDate){
                ageMonths--;
            }
        }
        else{
            ageMonths = currentMonth - birthMonth;
            if(currentDate < birthDate){
                ageMonths--;
            }
            
        }

        if(ageMonths == 0){
            $('#age').html(name +" is "+ ageYears +" years old");   
        }
        else if(ageYears == 0){
            $('#age').html(name +" is "+ ageMonths +" months old");
        }
        else{
            $('#age').html(name +" is "+ ageYears +" years and "+ ageMonths +" months old");
        }

    }

   
    
}


function validateInputs(name,birthYear,birthMonth,birthDate){

    var result = false;

    if(name == ""){
        $("#alert_name_req").html("Name Field is required");
        $(".alert_name_req").show().delay(3000).fadeOut();
         
        result = true;
    }
    console.log(!isNaN(birthYear) && (!isNaN(birthMonth)));
    if(isNaN(birthYear) || isNaN(birthMonth) || isNaN(birthDate) ){
        console.log("!isNaN(birthYear)");
        $("#alert_date_req").html("Date is required");
        $(".alert_date_req").show().delay(3000).fadeOut();
        result = true;
    }

    return result;

}
