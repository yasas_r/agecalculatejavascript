
$(document).ready(function() {
    
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    var pastYear = currentYear - 30; // years - shows in year drop down

    /**year drop down - upto current year*/
    for (let year = currentYear; year >= pastYear; year--) {  
        $('#year').append('<option value="'+year+'">'+year+'</option>');
    }
    /**append all 31 days initially for date drop down */
    for (let date = 1; date <= 31; date++) {
        $("#date").append($('<option>', {
            value: date,
            text: date.toString(),
            id: 'date'+date
        }));
    }

    $("#year").change(function() {
        $("#month").val("");
        $("#date").val("");

        var year = $(this).val();   
        var currentMonth  = parseInt(currentDate.getMonth()+1);

        setMonths(year,currentYear,currentMonth); 

        $('#month').removeAttr('disabled'); //month enable
        $("#date").prop( "disabled", true ); // dates disable
        
        $("#month").change(function() {
            $("#date").val("");
            var month = parseInt($(this).val());
            var currentDay = parseInt(currentDate.getDate());  

            setDates(year,month,currentYear,currentMonth,currentDay);
            
            $('#date').removeAttr('disabled');
        });
    });
    
});

/**checking current year with inputted year to disable next months */
function setMonths(year,currentYear,currentMonth){
    if(year == currentYear){
        for (let index = currentMonth+1; index <= 12; index++) {
            $('#month'+index+'').prop( "disabled", true );     
        }
    }
    else{
        for (let index =1; index <= 12; index++) {
            $('#month'+index+'').prop( "disabled", false );   
        }
    }
}

function setDates(year,month,currentYear,currentMonth,currentDay){
    /** hide and show date drop down according to inputted month*/
    if( [8,9,10,11,12].indexOf(month) == -1 ){   
        if(year % 4 == 0 && month == 2){
            $('#date29').show();
            $('#date30').hide();
            $('#date31').hide();
        } else if(month == 2){
            $('#date29').hide();
            $('#date30').hide();                
            $('#date31').hide();
        } else if(month % 2 == 0){
            $('#date29').show();
            $('#date30').show(); 
            $('#date31').hide();            
        }
        else{
            $('#date29').show();
            $('#date30').show(); 
            $('#date31').show();               
        }         
    }    
    else{
        if(month % 2 == 0){
            $('#date29').show();
            $('#date30').show(); 
            $('#date31').show();            
        }
        else{
            $('#date29').show();
            $('#date30').show();
            $('#date31').hide();               
        }
    }

    /**checking current month with inputted month to disable next days */
    if(year == currentYear && month == currentMonth){
        for (let index = currentDay+1; index <= 31; index++) {
            $('#date'+index+'').prop( "disabled", true );       
        }
    }
    else{
        for (let index = 1; index <= 31; index++) {
            $('#date'+index+'').prop( "disabled", false );        
        }
    }
}










