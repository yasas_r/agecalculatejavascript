
$(document).ready(function() {

    $("#name").on("keyup", function(){
        var regexp = /[^a-z A-Z]/g;
        if($(this).val().match(regexp)){
            $(this).val( $(this).val().replace(regexp,'') );
            $(this).val("");

            $("#alert_charactor").html("Name should be included only charact");
            $(".alert_charactor").show().delay(3000).fadeOut();
        }
    });

});